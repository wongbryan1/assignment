APP=isec3004.assignment

all: build run

build:
	docker build --rm --tag=$(APP) .
	docker image prune -f

run:
	docker run -p 0.0.0.0:5000:5000/tcp -it --rm $(APP)

clean:
	docker image rm $(APP)
	docker system prune

.PHONY: all test clean